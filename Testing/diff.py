# from genie.utils.diff import Diff
# diff = Diff('/home/storm/pyats/diff/core-pe1.txt', '/home/storm/pyats/diff/core-pe1_2.txt')
# diff.findDiff()
# print(diff)

import json
from genie.utils.diff import Diff

# Function to load a JSON file
def load_json(filepath):
    with open(filepath, 'r') as file:
        return json.load(file)

# Load the structured data from the files
data1 = load_json('/home/storm/pyats/diff/core-pe1.txt/interface_ios_MPLS-CORE-PE1_ops.txt')
data2 = load_json('/home/storm/pyats/diff/core-pe1_2.txt/interface_ios_MPLS-CORE-PE1_ops.txt')

# data1 = load_json('/home/storm/pyats/diff/core-pe1.txt/interface_ios_MPLS-CORE-PE1_console.txt')
# data2 = load_json('/home/storm/pyats/diff/core-pe1_2.txt/interface_ios_MPLS-CORE-PE1_console.txt')

# Create a Diff instance and find differences
diff = Diff(data1, data2)
diff.findDiff()

# Print the differences
print(diff)
