import logging
from pyats.topology import loader

# Configure logging
logging.basicConfig(filename='/home/storm/pyats/log/logfile.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

logger = logging.getLogger(__name__)

# Load the testbed and access the device
testbed = loader.load('/home/storm/pyats/eda.yaml')
device = testbed.devices['MPLS-CORE-PE1']

# Logging the device connection
logger.info(f'Connecting to {device.name}')
device.connect()

# Entering configuration mode and applying configuration
logger.info('Entering configuration mode and applying configuration')
device.configure('logging buffered 5000')

# Sending multiple configuration commands
config_commands = [
    'interface e0/2',
    'description Configured by pyATS_log',
    'no shutdown',
]
device.configure(config_commands)
logger.info(f'Applied configuration commands to {device.name}')

# Disconnecting from the device
logger.info(f'Disconnecting from {device.name}')
device.disconnect()
