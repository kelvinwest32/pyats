from pyats.topology import loader
import logging

# Load the testbed from a specified YAML file
testbed = loader.load('/home/storm/pyats/eda.yaml')

# Access a specific device from the testbed
device = testbed.devices['MPLS-CORE-PE1']

# Set up logging to an external file
logging.basicConfig(filename='/home/storm/pyats/log/debug_log.log', level=logging.DEBUG)

# Establish a connection to the device with debugging enabled
device.connect(log_stdout=False)

# Learn all available features and operational data from the device
all_features = device.learn('all')

# Iterate over the learned features and save each to a separate file
for feature, data in all_features.items():
    filename = f'/home/storm/pyats/learned/{feature}.txt'
    with open(filename, 'w') as file:
        file.write(str(data))
