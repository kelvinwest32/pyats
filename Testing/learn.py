from pyats.topology import loader

testbed = loader.load('/home/storm/pyats/eda.yaml')

device = testbed.devices['MPLS-CORE-PE1']

device.connect()
# device.connect(debug=True)

# device.execute('show version')

# parse output 
device.learn('ospf')
device.learn('bgp')
