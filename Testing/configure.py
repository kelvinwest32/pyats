from pyats.topology import loader

# Load the testbed and access the device
testbed = loader.load('/home/storm/pyats/eda.yaml')
device = testbed.devices['MPLS-CORE-PE1']
device.connect()

# Entering configuration mode
device.configure('logging buffered 5000')

# Sending multiple configuration commands
# You can pass a list of commands to the configure method to apply multiple configurations
config_commands = [
    'interface e0/2',
    'description Configured by pyATS',
    'no shutdown',
]
device.configure(config_commands)

# Exiting configuration mode is handled automatically when you use device.configure()

# Verify or save configuration if needed
# device.execute('write memory')

device.disconnect()
