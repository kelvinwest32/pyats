from pyats.topology import loader
from pprint import pprint
testbed = loader.load('/home/storm/pyats/Tier3mpls/coredevices.yaml')

device = testbed.devices['RR']

device.connect(log_stdout=False, learn_hostname=True)
# device.connect(debug=True)

# # using execute api
# sh_ver_exe = device.execute('show version')
# print(sh_ver_exe)

# # parse output 
# sh_ver_parsed = device.parse('show version')
# pprint(sh_ver_parsed)
# pprint(sh_ver_parsed['version']['uptime'])

# sh_ip_int_br = device.parse('show ip interface brief')
# int_parsed = sh_ip_int_br['interface']
# pprint(sh_ip_int_br)
# pprint(int_parsed)

# for int in int_parsed:
# 	print(f" Name: {int} ,  IP: {int_parsed[int]['ip_address']}, Status: {int_parsed[int]['status']}")

# # device.log.setLevel(logging.DEBUG)

# pprint(dir(device.api))
pprint(device.api.get_interface_ipv4_address())

# print(device.api.get_running_config_all())