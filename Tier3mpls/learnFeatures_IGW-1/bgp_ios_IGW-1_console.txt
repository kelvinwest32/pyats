+++ IGW-1 with via 'cli': executing command 'show bgp all summary' +++
show bgp all summary
For address family: VPNv4 Unicast
BGP router identifier 192.168.254.1, local AS number 65000
BGP table version is 2, main routing table version 2
1 network entries using 264 bytes of memory
2 path entries using 272 bytes of memory
2/1 BGP path/bestpath attribute entries using 624 bytes of memory
1 BGP AS-PATH entries using 24 bytes of memory
1 BGP extended community entries using 24 bytes of memory
0 BGP route-map cache entries using 0 bytes of memory
0 BGP filter-list cache entries using 0 bytes of memory
BGP using 1208 total bytes of memory
1 received paths for inbound soft reconfiguration
BGP activity 1/0 prefixes, 2/0 paths, scan interval 60 secs
1 networks peaked at 20:17:40 Apr 29 2024 UTC (11:34:42.424 ago)

Neighbor        V           AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
192.168.254.99  4        65000     770     768        2    0    0 11:34:57        0
197.168.254.1   4        65001     771     769        2    0    0 11:35:39        1
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip bgp template peer-session' +++
show ip bgp template peer-session
No templates configured

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip bgp template peer-policy' +++
show ip bgp template peer-policy
No templates configured

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show vrf detail | inc \(VRF' +++
show vrf detail | inc \(VRF
VRF INTERNET (VRF Id = 1); default RD 400:400; default VPNID <not set>
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all cluster-ids' +++
show bgp all cluster-ids
Global cluster-id: 192.168.254.1 (configured: 0.0.0.0)
BGP client-to-client reflection:         Configured    Used
  all (inter-cluster and intra-cluster): ENABLED
  intra-cluster:                         ENABLED       ENABLED

List of cluster-ids:
Cluster-id     #-neighbors C2C-rfl-CFG C2C-rfl-USE
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip bgp all dampening parameters' +++
show ip bgp all dampening parameters
For address family: IPv4 Unicast

% dampening not enabled for base

For address family: VPNv4 Unicast

% dampening not enabled for base

For vrf: INTERNET

% dampening not enabled for vrf INTERNET

For address family: IPv4 Multicast

% dampening not enabled for base

For address family: L2VPN E-VPN

% dampening not enabled for base

For address family: VPNv4 Multicast

% dampening not enabled for base

For vrf: INTERNET

% dampening not enabled for vrf INTERNET

For address family: MVPNv4 Unicast

% dampening not enabled for base

For vrf: INTERNET

% dampening not enabled for vrf INTERNET

For address family: VPNv4 Flowspec

% dampening not enabled for base

For vrf: INTERNET

% dampening not enabled for vrf INTERNET
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors' +++
show bgp all neighbors
For address family: IPv4 Unicast

For address family: VPNv4 Unicast
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
  BGP version 4, remote router ID 192.168.254.99
  BGP state = Established, up for 11:34:58
  Last read 00:00:24, last write 00:00:01, hold time is 180, keepalive interval is 60 seconds
  Last update received: 11:33:57
  Neighbor sessions:
    1 active, is not multisession capable (disabled)
  Neighbor capabilities:
    Route refresh: advertised and received(new)
    Four-octets ASN Capability: advertised and received
    Address family VPNv4 Unicast: advertised and received
    Enhanced Refresh Capability: advertised and received
    Extended Next Hop Encoding Capability:
        VPNv4 Unicast: advertised and received
    Multisession Capability: 
    Stateful switchover support enabled: NO for session 1
  Message statistics:
    InQ depth is 0
    OutQ depth is 0
    
                         Sent       Rcvd
    Opens:                  1          1
    Notifications:          0          0
    Updates:                2          2
    Keepalives:           765        767
    Route Refresh:          0          0
    Total:                768        770
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 0 seconds

  Address tracking is enabled, the RIB does have a route to 192.168.254.99
  Route to peer address reachability Up: 1; Down: 0
    Last notification 11:35:07
  Connections established 1; dropped 0
  Last reset never
  Interface associated: (none) (peering address NOT in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
Connection state is ESTAB, I/O status: 1, unread input bytes: 0            
Connection is ECN Disabled, Mininum incoming TTL 0, Outgoing TTL 255
Local host: 192.168.254.1, Local port: 17245
Foreign host: 192.168.254.99, Foreign port: 179
Connection tableid (VRF): 0
Maximum output segment queue size: 50

Enqueued packets for retransmit: 0, input: 0  mis-ordered: 0 (0 bytes)

Event Timers (current time is 0x27D94CE):
Timer          Starts    Wakeups            Next
Retrans           768          0             0x0
TimeWait            0          0             0x0
AckHold           769        763             0x0
SendWnd             0          0             0x0
KeepAlive           0          0             0x0
GiveUp              0          0             0x0
PmtuAger        40669      40668       0x27D98B6
DeadWait            0          0             0x0
Linger              0          0             0x0
ProcessQ            0          0             0x0

iss: 1778549222  snduna: 1778563947  sndnxt: 1778563947
irs: 3388609212  rcvnxt: 3388623989

sndwnd:  15719  scale:      0  maxrcvwnd:  16384
rcvwnd:  15662  scale:      0  delrcvwnd:    722

SRTT: 1000 ms, RTTO: 1003 ms, RTV: 3 ms, KRTT: 0 ms
minRTT: 1 ms, maxRTT: 1000 ms, ACK hold: 120 ms
uptime: 41698725 ms, Sent idletime: 1945 ms, Receive idletime: 1824 ms 
Status Flags: active open
Option Flags: nagle, path mtu capable, SACK option permitted
  win-scale
IP Precedence value : 6
Window update Optimisation : Enabled
ACK Optimisation : Dynamic ACK Tuning Enabled

Datagrams (max data segment is 1396 bytes):
Peer MSS:       1396
Rcvd: 1535 (out of order: 0), with data: 769, total data bytes: 14776
Sent: 1537 (retransmit: 0, fastretransmit: 0, partialack: 0, Second Congestion: 0), with data: 768, total data bytes: 14724

 Packets received in fast path: 0, fast processed: 0, slow path: 0
 fast lock acquisition failures: 0, slow path: 0
TCP Semaphore      0x7F2ABD4976B8  FREE 

BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  BGP version 4, remote router ID 197.168.254.1
  BGP state = Established, up for 11:35:40
  Last read 00:00:23, last write 00:00:06, hold time is 180, keepalive interval is 60 seconds
  Last update received: 11:34:43
  Neighbor sessions:
    1 active, is not multisession capable (disabled)
  Neighbor capabilities:
    Route refresh: advertised and received(new)
    Four-octets ASN Capability: advertised and received
    Address family IPv4 Unicast: advertised and received
    Enhanced Refresh Capability: advertised and received
    Multisession Capability: 
    Stateful switchover support enabled: NO for session 1
  Message statistics:
    InQ depth is 0
    OutQ depth is 0
    
                         Sent       Rcvd
    Opens:                  1          1
    Notifications:          0          0
    Updates:                1          2
    Keepalives:           767        768
    Route Refresh:          0          0
    Total:                769        771
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 0 seconds

  Address tracking is enabled, the RIB does have a route to 197.168.254.1
  Route to peer address reachability Up: 1; Down: 0
    Last notification 11:35:55
  Connections established 1; dropped 0
  Last reset never
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
  Interface associated: Ethernet0/1 (peering address in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
Connection state is ESTAB, I/O status: 1, unread input bytes: 0            
Connection is ECN Disabled, Mininum incoming TTL 0, Outgoing TTL 1
Local host: 197.168.254.2, Local port: 179
Foreign host: 197.168.254.1, Foreign port: 38062
Connection tableid (VRF): 1
Maximum output segment queue size: 50

Enqueued packets for retransmit: 0, input: 0  mis-ordered: 0 (0 bytes)

Event Timers (current time is 0x27D94CF):
Timer          Starts    Wakeups            Next
Retrans           769          0             0x0
TimeWait            0          0             0x0
AckHold           770        763             0x0
SendWnd             0          0             0x0
KeepAlive           0          0             0x0
GiveUp              0          0             0x0
PmtuAger            0          0             0x0
DeadWait            0          0             0x0
Linger              0          0             0x0
ProcessQ            0          0             0x0

iss: 3778355608  snduna: 3778370262  sndnxt: 3778370262
irs: 1470505360  rcvnxt: 1470520077

sndwnd:  16365  scale:      0  maxrcvwnd:  16384
rcvwnd:  16308  scale:      0  delrcvwnd:     76

SRTT: 1000 ms, RTTO: 1003 ms, RTV: 3 ms, KRTT: 0 ms
minRTT: 1 ms, maxRTT: 1000 ms, ACK hold: 120 ms
uptime: 41740966 ms, Sent idletime: 6048 ms, Receive idletime: 5927 ms 
Status Flags: passive open, gen tcbs
Option Flags: VRF id set, nagle, path mtu capable, SACK option permitted
  win-scale
IP Precedence value : 6
Window update Optimisation : Enabled
ACK Optimisation : Dynamic ACK Tuning Enabled

Datagrams (max data segment is 1460 bytes):
Peer MSS:       1460
Rcvd: 1539 (out of order: 0), with data: 770, total data bytes: 14716
Sent: 1537 (retransmit: 0, fastretransmit: 0, partialack: 0, Second Congestion: 0), with data: 769, total data bytes: 14653

 Packets received in fast path: 0, fast processed: 0, slow path: 0
 fast lock acquisition failures: 0, slow path: 0
TCP Semaphore      0x7F2ABD497858  FREE 


For address family: IPv4 Multicast

For address family: L2VPN E-VPN

For address family: VPNv4 Multicast

For address family: MVPNv4 Unicast

For address family: VPNv4 Flowspec

For address family: IPv4 Label-Unicast
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 192.168.254.99 policy' +++
show bgp all neighbors 192.168.254.99 policy
 Neighbor: 192.168.254.99, Address-Family: VPNv4 Unicast

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 197.168.254.1 policy' +++
show bgp all neighbors 197.168.254.1 policy
 Neighbor: 197.168.254.1, Address-Family: VPNv4 Unicast (INTERNET)
 Locally configured policies:
  route-map internet-out out
  soft-reconfiguration inbound

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all' +++
show bgp all
For address family: IPv4 Unicast


For address family: VPNv4 Unicast

BGP table version is 2, local router ID is 192.168.254.1
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, L long-lived-stale,
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
Route Distinguisher: 400:400 (default for vrf INTERNET)
 *>   0.0.0.0          197.168.254.1                          0 65001 i

For address family: IPv4 Multicast


For address family: L2VPN E-VPN


For address family: VPNv4 Multicast


For address family: MVPNv4 Unicast


For address family: VPNv4 Flowspec

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all detail' +++
show bgp all detail
For address family: IPv4 Unicast


For address family: VPNv4 Unicast


Route Distinguisher: 400:400 (default for vrf INTERNET)
BGP routing table entry for 400:400:0.0.0.0/0, version 2
  Paths: (2 available, best #1, table INTERNET)
  Advertised to update-groups:
     2         
  Refresh Epoch 1
  65001
    197.168.254.1 (via vrf INTERNET) from 197.168.254.1 (197.168.254.1)
      Origin IGP, localpref 100, valid, external, best
      Extended Community: RT:400:400
      mpls labels in/out 915/nolabel
      rx pathid: 0, tx pathid: 0x0
      Updated on Apr 29 2024 20:17:40 UTC
  Refresh Epoch 1
  65001, (received-only)
    197.168.254.1 (via vrf INTERNET) from 197.168.254.1 (197.168.254.1)
      Origin IGP, localpref 100, valid, external
      mpls labels in/out 915/nolabel
      rx pathid: 0, tx pathid: 0
      Updated on Apr 29 2024 20:17:40 UTC

For address family: IPv4 Multicast


For address family: L2VPN E-VPN


For address family: VPNv4 Multicast


For address family: MVPNv4 Unicast


For address family: VPNv4 Flowspec

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 192.168.254.99 advertised-routes' +++
show bgp all neighbors 192.168.254.99 advertised-routes
For address family: VPNv4 Unicast
BGP table version is 2, local router ID is 192.168.254.1
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, L long-lived-stale,
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
Route Distinguisher: 400:400 (default for vrf INTERNET)
 *>   0.0.0.0          197.168.254.1                          0 65001 i

Total number of prefixes 1 
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 197.168.254.1 advertised-routes' +++
show bgp all neighbors 197.168.254.1 advertised-routes
For address family: VPNv4 Unicast

Total number of prefixes 0 
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 192.168.254.99 routes' +++
show bgp all neighbors 192.168.254.99 routes
For address family: VPNv4 Unicast

Total number of prefixes 0 
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 197.168.254.1 routes' +++
show bgp all neighbors 197.168.254.1 routes
For address family: VPNv4 Unicast
BGP table version is 2, local router ID is 192.168.254.1
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, L long-lived-stale,
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
Route Distinguisher: 400:400 (default for vrf INTERNET)
 *>   0.0.0.0          197.168.254.1                          0 65001 i

Total number of prefixes 1 
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 192.168.254.99 received-routes' +++
show bgp all neighbors 192.168.254.99 received-routes
For address family: VPNv4 Unicast
% Inbound soft reconfiguration not enabled on 192.168.254.99
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors 197.168.254.1 received-routes' +++
show bgp all neighbors 197.168.254.1 received-routes
For address family: VPNv4 Unicast
BGP table version is 2, local router ID is 192.168.254.1
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, L long-lived-stale,
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
Route Distinguisher: 400:400 (default for vrf INTERNET)
 *    0.0.0.0          197.168.254.1                          0 65001 i

Total number of prefixes 1 
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.168.254.99,  remote AS 65000, internal link
BGP neighbor is 197.168.254.1,  vrf INTERNET,  remote AS 65001, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
IGW-1#
+====================================================================================================================================================+
| Commands for learning feature 'Bgp'                                                                                                                |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllSummary'>, arguments: {'address_family':'','vrf':''}                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllClusterIds'>                                                                               |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighbors'>, arguments: {'address_family':'','neighbor':''}                                |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'197.168.254.1'}                                 |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAll'>, arguments: {'address_family':''}                                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllDetail'>, arguments: {'address_family':'','vrf':''}                                        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'192.168.254.99'}  |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'192.168.254.99'}            |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'197.168.254.1'}             |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'197.168.254.1'}     |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpTemplatePeerSession'>                                                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpTemplatePeerPolicy'>                                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpAllDampeningParameters'>                                                                    |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'192.168.254.99'}                                |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'197.168.254.1'}   |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'192.168.254.99'}    |
|====================================================================================================================================================|
