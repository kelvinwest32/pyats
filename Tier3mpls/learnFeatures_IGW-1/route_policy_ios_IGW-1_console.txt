+++ IGW-1 with via 'cli': executing command 'show route-map all' +++
show route-map all
STATIC routemaps
route-map internet-out, permit, sequence 10
  Match clauses:
    ip address prefix-lists: internet-out 
  Set clauses:
  Policy routing matches: 0 packets, 0 bytes
DYNAMIC routemaps
Current active dynamic routemaps = 0
IGW-1#
+====================================================================================================================================================+
| Commands for learning feature 'RoutePolicy'                                                                                                        |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_route_map.ShowRouteMapAll'>                                                                              |
|====================================================================================================================================================|
