from pyats.topology import loader
from pprint import pprint
import json 

tb = loader.load('/home/storm/pyats/Tier3mpls/coredevices.yaml')
dev = tb.devices['RR']
dev.connect(log_stdout=False, learn_hostname=True)

# learn features
learn_int = dev.learn("interface")
# get mtehods from the  object in a dictionary format
# pprint(learn_int.to_dict())
ospf_data = learn_int.to_dict()
# print(type(learn_int))
# pprint(dir(learn_int))

# learn_route = dev.learn("static_routing")
# print(learn_route.to_dict())

 # Write OSPF data to a JSON file
with open('ospf_data.json', 'w') as file:
    json.dump(ospf_data, file, indent=4)