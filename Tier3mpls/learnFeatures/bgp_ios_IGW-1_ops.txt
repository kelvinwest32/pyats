{
  "_exclude": [
    "if_handle",
    "keepalives",
    "last_reset",
    "reset_reason",
    "foreign_port",
    "local_port",
    "msg_rcvd",
    "msg_sent",
    "up_down",
    "bgp_table_version",
    "routing_table_version",
    "tbl_ver",
    "table_version",
    "memory_usage",
    "updates",
    "mss",
    "total",
    "total_bytes",
    "up_time",
    "bgp_negotiated_keepalive_timers",
    "hold_time",
    "keepalive_interval",
    "sent",
    "received",
    "status_codes",
    "holdtime",
    "router_id",
    "connections_dropped",
    "connections_established",
    "advertised",
    "prefixes",
    "routes",
    "state_pfxrcd"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "instance": {
      "default": {
        "bgp_id": 65000,
        "vrf": {
          "INTERNET": {
            "cluster_id": "192.168.254.1",
            "neighbor": {
              "197.168.254.1": {
                "address_family": {
                  "vpnv4 unicast": {
                    "route_map_name_out": "internet-out"
                  }
                },
                "bgp_negotiated_capabilities": {
                  "enhanced_refresh": "advertised and received",
                  "four_octets_asn": "advertised and received",
                  "route_refresh": "advertised and received(new)",
                  "stateful_switchover": "NO for session 1"
                },
                "bgp_negotiated_keepalive_timers": {
                  "hold_time": 180,
                  "keepalive_interval": 60
                },
                "bgp_neighbor_counters": {
                  "messages": {
                    "received": {
                      "keepalives": 115,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 2
                    },
                    "sent": {
                      "keepalives": 113,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 1
                    }
                  }
                },
                "bgp_session_transport": {
                  "connection": {
                    "last_reset": "never",
                    "state": "Established"
                  },
                  "transport": {
                    "foreign_host": "197.168.254.1",
                    "foreign_port": "38062",
                    "local_host": "197.168.254.2",
                    "local_port": "179",
                    "mss": 1460
                  }
                },
                "bgp_version": 4,
                "remote_as": 65001,
                "session_state": "Established",
                "shutdown": false
              }
            }
          },
          "default": {
            "cluster_id": "192.168.254.1",
            "neighbor": {
              "192.168.254.99": {
                "address_family": {
                  "vpnv4 unicast": {
                    "bgp_table_version": 2,
                    "path": {
                      "memory_usage": 272,
                      "total_entries": 2
                    },
                    "prefixes": {
                      "memory_usage": 264,
                      "total_entries": 1
                    },
                    "routing_table_version": 2,
                    "total_memory": 1208
                  }
                },
                "bgp_negotiated_capabilities": {
                  "enhanced_refresh": "advertised and received",
                  "four_octets_asn": "advertised and received",
                  "route_refresh": "advertised and received(new)",
                  "stateful_switchover": "NO for session 1",
                  "vpnv4_unicast": "advertised and received"
                },
                "bgp_negotiated_keepalive_timers": {
                  "hold_time": 180,
                  "keepalive_interval": 60
                },
                "bgp_neighbor_counters": {
                  "messages": {
                    "received": {
                      "keepalives": 113,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 2
                    },
                    "sent": {
                      "keepalives": 112,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 2
                    }
                  }
                },
                "bgp_session_transport": {
                  "connection": {
                    "last_reset": "never",
                    "state": "Established"
                  },
                  "transport": {
                    "foreign_host": "192.168.254.99",
                    "foreign_port": "179",
                    "local_host": "192.168.254.1",
                    "local_port": "17245",
                    "mss": 1396
                  }
                },
                "bgp_version": 4,
                "remote_as": 65000,
                "session_state": "Established",
                "shutdown": false
              },
              "197.168.254.1": {
                "address_family": {
                  "vpnv4 unicast": {
                    "bgp_table_version": 2,
                    "path": {
                      "memory_usage": 272,
                      "total_entries": 2
                    },
                    "prefixes": {
                      "memory_usage": 264,
                      "total_entries": 1
                    },
                    "routing_table_version": 2,
                    "total_memory": 1208
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "raw_data": false,
  "routes_per_peer": {
    "instance": {
      "default": {
        "vrf": {
          "INTERNET": {
            "neighbor": {
              "197.168.254.1": {
                "address_family": {
                  "vpnv4 unicast": {
                    "received_routes": {},
                    "routes": {}
                  },
                  "vpnv4 unicast RD 400:400": {
                    "received_routes": {},
                    "routes": {
                      "0.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "197.168.254.1",
                            "origin_codes": "i",
                            "path": "65001",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      }
                    }
                  }
                },
                "remote_as": 65001
              }
            }
          },
          "default": {
            "neighbor": {
              "192.168.254.99": {
                "address_family": {
                  "vpnv4 unicast": {
                    "advertised": {},
                    "input_queue": 0,
                    "msg_rcvd": 116,
                    "msg_sent": 115,
                    "output_queue": 0,
                    "state_pfxrcd": "0",
                    "tbl_ver": 2,
                    "up_down": "01:42:15"
                  },
                  "vpnv4 unicast RD 400:400": {
                    "advertised": {
                      "0.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "197.168.254.1",
                            "origin_codes": "i",
                            "path": "65001",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      }
                    },
                    "default_vrf": "INTERNET",
                    "route_distinguisher": "400:400"
                  }
                },
                "remote_as": 65000
              },
              "197.168.254.1": {
                "address_family": {
                  "vpnv4 unicast": {
                    "input_queue": 0,
                    "msg_rcvd": 118,
                    "msg_sent": 115,
                    "output_queue": 0,
                    "state_pfxrcd": "1",
                    "tbl_ver": 2,
                    "up_down": "01:42:57"
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "table": {
    "instance": {
      "default": {
        "vrf": {
          "INTERNET": {
            "address_family": {
              "vpnv4 unicast RD 400:400": {
                "bgp_table_version": 2,
                "default_vrf": "INTERNET",
                "prefixes": {
                  "0.0.0.0/0": {
                    "index": {
                      "1": {
                        "gateway": "197.168.254.1",
                        "localpref": 100,
                        "next_hop": "197.168.254.1",
                        "origin_codes": "i",
                        "originator": "197.168.254.1",
                        "status_codes": "*>",
                        "update_group": 2
                      },
                      "2": {
                        "gateway": "197.168.254.1",
                        "localpref": 100,
                        "next_hop": "197.168.254.1",
                        "origin_codes": "i",
                        "originator": "197.168.254.1",
                        "status_codes": "* ",
                        "update_group": 2
                      }
                    },
                    "paths": "2 available, best #1, table INTERNET",
                    "table_version": "2"
                  }
                },
                "route_distinguisher": "400:400",
                "route_identifier": "192.168.254.1"
              }
            }
          }
        }
      }
    }
  }
}