+++ IGW-1 with via 'cli': executing command 'show vrf detail' +++
show vrf detail
VRF INTERNET (VRF Id = 1); default RD 400:400; default VPNID <not set>
  New CLI format, supports multiple address-families
  Flags: 0x180C
  Interfaces:
    Et0/1                   
Address family ipv4 unicast (Table ID = 0x1):
  Flags: 0x0
  Export VPN route-target communities
    RT:400:400              
  Import VPN route-target communities
    RT:400:400              
  No import route-map
  No global export route-map
  No export route-map
  VRF label distribution protocol: not configured
  VRF label allocation mode: per-prefix
Address family ipv6 unicast not active
Address family ipv4 multicast not active
Address family ipv6 multicast not active

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip multicast vrf INTERNET' +++
show ip multicast vrf INTERNET
% Multicast not enabled for vrf INTERNET
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ipv6 pim vrf INTERNET interface' +++
show ipv6 pim vrf INTERNET interface
show ipv6 pim vrf INTERNET interface
                  ^
% Invalid input detected at '^' marker.

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip mroute vrf INTERNET static' +++
show ip mroute vrf INTERNET static
% Multicast not enabled for vrf INTERNET
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip mroute vrf INTERNET' +++
show ip mroute vrf INTERNET
% Multicast not enabled for vrf INTERNET
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ipv6 mroute vrf INTERNET' +++
show ipv6 mroute vrf INTERNET
show ipv6 mroute vrf INTERNET
                     ^
% Invalid input detected at '^' marker.

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip multicast' +++
show ip multicast
  Multicast Routing: disabled
  Multicast Multipath: disabled
  Multicast Route limit: No limit
  Multicast Fallback group mode: Sparse
  Number of multicast boundaries configured with filter-autorp option: 0
  MoFRR: Disabled
  Multicast Service-Reflect Capabilities:
	Unicast to Multicast
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ipv6 pim interface' +++
show ipv6 pim interface
No interfaces found.

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip mroute static' +++
show ip mroute static
IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ip mroute' +++
show ip mroute
IP Multicast Routing Table
Flags: D - Dense, S - Sparse, B - Bidir Group, s - SSM Group, C - Connected,
       L - Local, P - Pruned, R - RP-bit set, F - Register flag,
       T - SPT-bit set, J - Join SPT, M - MSDP created entry, E - Extranet,
       X - Proxy Join Timer Running, A - Candidate for MSDP Advertisement,
       U - URD, I - Received Source Specific Host Report, 
       Z - Multicast Tunnel, z - MDT-data group sender, 
       Y - Joined MDT-data group, y - Sending to MDT-data group, 
       G - Received BGP C-Mroute, g - Sent BGP C-Mroute, 
       N - Received BGP Shared-Tree Prune, n - BGP C-Mroute suppressed, 
       Q - Received BGP S-A Route, q - Sent BGP S-A Route, 
       V - RD & Vector, v - Vector, p - PIM Joins on route, 
       x - VxLAN group, c - PFP-SA cache created entry, 
       * - determined by Assert, # - iif-starg configured on rpf intf, 
       e - encap-helper tunnel flag, l - LISP decap ref count contributor
Outgoing interface flags: H - Hardware switched, A - Assert winner, p - PIM Join
                          t - LISP transit group
 Timers: Uptime/Expires
 Interface state: Interface, Next-Hop or VCD, State/Mode

IP Multicast Forwarding is not enabled.

IGW-1#
+++ IGW-1 with via 'cli': executing command 'show ipv6 mroute' +++
show ipv6 mroute
No mroute entries found.

IGW-1#
====|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMulticast'>, arguments: {'vrf':'INTERNET'}                                                   |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMrouteStatic'>, arguments: {'vrf':'INTERNET'}                                                |
|====================================================================================================================================================|
Could not learn <class 'genie.libs.parser.iosxe.show_pim.ShowIpv6PimInterface'>
Parser Output is empty
Could not learn <class 'genie.libs.parser.ios.show_mcast.ShowIpMrouteStatic'>
Show Command: show ip mroute vrf default static
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Mcast'                                                                                                              |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMulticast'>, arguments: {'vrf':''}                                                           |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMroute'>, arguments: {'vrf':''}                                                              |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpv6Mroute'>, arguments: {'vrf':''}                                                            |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.iosxe.show_pim.ShowIpv6PimInterface'>, arguments: {'vrf':''}                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMrouteStatic'>, arguments: {'vrf':''}                                                        |
|====================================================================================================================================================|
