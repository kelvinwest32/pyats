+++ MPLS-CORE-PE1 with via 'cli': executing command 'show vrf detail' +++
show vrf detail
VRF ford (VRF Id = 1); default RD 400:400; default VPNID <not set>
  New CLI format, supports multiple address-families
  Flags: 0x180C
  Interfaces:
    Et0/1.1289              
Address family ipv4 unicast (Table ID = 0x1):
  Flags: 0x0
  Export VPN route-target communities
    RT:400:400              
  Import VPN route-target communities
    RT:400:400              
  No import route-map
  No global export route-map
  No export route-map
  VRF label distribution protocol: not configured
  VRF label allocation mode: per-prefix
Address family ipv6 unicast not active
Address family ipv4 multicast not active

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip mroute' +++
show ip mroute
IP Multicast Forwarding is not enabled.
IP Multicast Routing Table
Flags: D - Dense, S - Sparse, B - Bidir Group, s - SSM Group, C - Connected,
       L - Local, P - Pruned, R - RP-bit set, F - Register flag,
       T - SPT-bit set, J - Join SPT, M - MSDP created entry, E - Extranet,
       X - Proxy Join Timer Running, A - Candidate for MSDP Advertisement,
       U - URD, I - Received Source Specific Host Report, 
       Z - Multicast Tunnel, z - MDT-data group sender, 
       Y - Joined MDT-data group, y - Sending to MDT-data group, 
       G - Received BGP C-Mroute, g - Sent BGP C-Mroute, 
       N - Received BGP Shared-Tree Prune, n - BGP C-Mroute suppressed, 
       Q - Received BGP S-A Route, q - Sent BGP S-A Route, 
       V - RD & Vector, v - Vector, p - PIM Joins on route, 
       x - VxLAN group
Outgoing interface flags: H - Hardware switched, A - Assert winner, p - PIM Join
 Timers: Uptime/Expires
 Interface state: Interface, Next-Hop or VCD, State/Mode

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 mroute' +++
show ipv6 mroute
No mroute entries found.

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim rp mapping' +++
show ip pim rp mapping
PIM Group-to-RP Mappings

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim bsr-router' +++
show ip pim bsr-router
PIMv2 Bootstrap information
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 pim bsr election' +++
show ipv6 pim bsr election
No BSR information found 

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 pim bsr candidate-rp' +++
show ipv6 pim bsr candidate-rp
No C-RP information found 

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim interface df' +++
show ip pim interface df
* implies this system is the DF
Interface                RP               DF Winner        Metric     Uptime
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim interface detail' +++
show ip pim interface detail
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim interface' +++
show ip pim interface

Address          Interface                Ver/   Nbr    Query  DR         DR
                                          Mode   Count  Intvl  Prior
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 pim interface' +++
show ipv6 pim interface
No interfaces found.

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim neighbor' +++
show ip pim neighbor
PIM Neighbor Table
Mode: B - Bidir Capable, DR - Designated Router, N - Default DR Priority,
      P - Proxy Capable, S - State Refresh Capable, G - GenID Capable,
      L - DR Load-balancing Capable
Neighbor          Interface                Uptime/Expires    Ver   DR
Address                                                            Prio/Mode
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 pim neighbor detail' +++
show ipv6 pim neighbor detail
No neighbors found.

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip mroute vrf ford' +++
show ip mroute vrf ford
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim vrf ford rp mapping' +++
show ip pim vrf ford rp mapping
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim vrf ford bsr-router' +++
show ip pim vrf ford bsr-router
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim vrf ford interface df' +++
show ip pim vrf ford interface df
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim vrf ford interface detail' +++
show ip pim vrf ford interface detail
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim vrf ford interface' +++
show ip pim vrf ford interface
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip pim vrf ford neighbor' +++
show ip pim vrf ford neighbor
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
Could not learn <class 'genie.libs.parser.ios.show_pim.ShowIpPimNeighbor'>
Show Command: show ip pim vrf ford neighbor
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Pim'                                                                                                                |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_vrf.ShowVrfDetail'>                                                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMroute'>, arguments: {'vrf':''}                                                              |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpv6Mroute'>, arguments: {'vrf':''}                                                            |
|   cmd: <class 'genie.libs.parser.ios.show_mcast.ShowIpMroute'>, arguments: {'vrf':'ford'}                                                          |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimRpMapping'>, arguments: {'vrf':''}                                                          |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimBsrRouter'>, arguments: {'vrf':''}                                                          |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpv6PimBsrElection'>, arguments: {'vrf':''}                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpv6PimBsrCandidateRp'>, arguments: {'vrf':''}                                                   |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimInterfaceDf'>, arguments: {'vrf':''}                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimInterfaceDetail'>, arguments: {'vrf':''}                                                    |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimInterface'>, arguments: {'vrf':''}                                                          |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpv6PimInterface'>, arguments: {'vrf':''}                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimNeighbor'>, arguments: {'vrf':''}                                                           |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpv6PimNeighborDetail'>, arguments: {'vrf':''}                                                   |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimRpMapping'>, arguments: {'vrf':'ford'}                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimBsrRouter'>, arguments: {'vrf':'ford'}                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimInterfaceDf'>, arguments: {'vrf':'ford'}                                                    |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimInterfaceDetail'>, arguments: {'vrf':'ford'}                                                |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimInterface'>, arguments: {'vrf':'ford'}                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_pim.ShowIpPimNeighbor'>, arguments: {'vrf':'ford'}                                                       |
|====================================================================================================================================================|
