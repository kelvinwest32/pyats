+++ MPLS-CORE-PE1 with via 'cli': executing command 'show vrf detail' +++
show vrf detail
VRF ford (VRF Id = 1); default RD 400:400; default VPNID <not set>
  New CLI format, supports multiple address-families
  Flags: 0x180C
  Interfaces:
    Et0/1.1289              
Address family ipv4 unicast (Table ID = 0x1):
  Flags: 0x0
  Export VPN route-target communities
    RT:400:400              
  Import VPN route-target communities
    RT:400:400              
  No import route-map
  No global export route-map
  No export route-map
  VRF label distribution protocol: not configured
  VRF label allocation mode: per-prefix
Address family ipv6 unicast not active
Address family ipv4 multicast not active

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip igmp interface' +++
show ip igmp interface
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip igmp groups detail' +++
show ip igmp groups detail

Flags: L - Local, U - User, SG - Static Group, VG - Virtual Group,
       SS - Static Source, VS - Virtual Source,
       Ac - Group accounted towards access control limit
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip igmp vrf ford interface' +++
show ip igmp vrf ford interface
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip igmp vrf ford groups detail' +++
show ip igmp vrf ford groups detail
% Multicast not enabled for vrf ford
MPLS-CORE-PE1#
Could not learn <class 'genie.libs.parser.ios.show_igmp.ShowIpIgmpGroupsDetail'>
Show Command: show ip igmp vrf ford groups detail
Parser Output is empty
