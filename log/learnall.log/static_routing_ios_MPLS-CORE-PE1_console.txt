+++ MPLS-CORE-PE1 with via 'cli': executing command 'show vrf detail' +++
show vrf detail
VRF ford (VRF Id = 1); default RD 400:400; default VPNID <not set>
  New CLI format, supports multiple address-families
  Flags: 0x180C
  Interfaces:
    Et0/1.1289              
Address family ipv4 unicast (Table ID = 0x1):
  Flags: 0x0
  Export VPN route-target communities
    RT:400:400              
  Import VPN route-target communities
    RT:400:400              
  No import route-map
  No global export route-map
  No export route-map
  VRF label distribution protocol: not configured
  VRF label allocation mode: per-prefix
Address family ipv6 unicast not active
Address family ipv4 multicast not active

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip static route vrf ford' +++
show ip static route vrf ford
Codes: M - Manual static, A - AAA download, N - IP NAT, D - DHCP,
       G - GPRS, V - Crypto VPN, C - CASA, P - Channel interface processor,
       B - BootP, S - Service selection gateway
       DN - Default Network, T - Tracking object
       L - TL1, E - OER, I - iEdge
       D1 - Dot1x Vlan Network, K - MWAM Route
       PP - PPP default route, MR - MRIPv6, SS - SSLVPN
       H - IPe Host, ID - IPe Domain Broadcast
       U - User GPRS, TE - MPLS Traffic-eng, LI - LIIN
       IR - ICMP Redirect
Codes in []: A - active, N - non-active, B - BFD-tracked, D - Not Tracked, P - permanent

Static local RIB for ford 

M  192.168.200.1/32 [1/0] via 10.22.80.2 [A]
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip static route' +++
show ip static route
Codes: M - Manual static, A - AAA download, N - IP NAT, D - DHCP,
       G - GPRS, V - Crypto VPN, C - CASA, P - Channel interface processor,
       B - BootP, S - Service selection gateway
       DN - Default Network, T - Tracking object
       L - TL1, E - OER, I - iEdge
       D1 - Dot1x Vlan Network, K - MWAM Route
       PP - PPP default route, MR - MRIPv6, SS - SSLVPN
       H - IPe Host, ID - IPe Domain Broadcast
       U - User GPRS, TE - MPLS Traffic-eng, LI - LIIN
       IR - ICMP Redirect
Codes in []: A - active, N - non-active, B - BFD-tracked, D - Not Tracked, P - permanent


MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 static vrf ford detail' +++
show ipv6 static vrf ford detail
% IPv6 routing table ford does not exist
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 static detail' +++
show ipv6 static detail
IPv6 Static routes Table - default
Codes: * - installed in RIB, u/m - Unicast/Multicast only
       U - Per-user Static route
       N - ND Static route
       M - MIP Static route
       P - DHCP-PD Static route
       R - RHI Static route
MPLS-CORE-PE1#
+====================================================================================================================================================+
| Commands for learning feature 'StaticRouting'                                                                                                      |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.iosxe.show_vrf.ShowVrfDetail'>                                                                                    |
|   cmd: <class 'genie.libs.parser.ios.show_static_routing.ShowIpStaticRoute'>, arguments: {'vrf':'ford'}                                            |
|   cmd: <class 'genie.libs.parser.ios.show_static_routing.ShowIpv6StaticDetail'>, arguments: {'vrf':''}                                             |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_static_routing.ShowIpStaticRoute'>, arguments: {'vrf':''}                                                |
|   cmd: <class 'genie.libs.parser.ios.show_static_routing.ShowIpv6StaticDetail'>, arguments: {'vrf':'ford'}                                         |
|====================================================================================================================================================|
