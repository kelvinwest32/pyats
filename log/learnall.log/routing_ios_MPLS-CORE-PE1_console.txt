+++ MPLS-CORE-PE1 with via 'cli': executing command 'show vrf detail' +++
show vrf detail
VRF ford (VRF Id = 1); default RD 400:400; default VPNID <not set>
  New CLI format, supports multiple address-families
  Flags: 0x180C
  Interfaces:
    Et0/1.1289              
Address family ipv4 unicast (Table ID = 0x1):
  Flags: 0x0
  Export VPN route-target communities
    RT:400:400              
  Import VPN route-target communities
    RT:400:400              
  No import route-map
  No global export route-map
  No export route-map
  VRF label distribution protocol: not configured
  VRF label allocation mode: per-prefix
Address family ipv6 unicast not active
Address family ipv4 multicast not active

MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip route vrf ford' +++
show ip route vrf ford

Routing Table: ford
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area 
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override, p - overrides from PfR

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 3 subnets, 2 masks
C        10.22.80.0/30 is directly connected, Ethernet0/1.1289
L        10.22.80.1/32 is directly connected, Ethernet0/1.1289
B        10.22.80.8/30 [200/0] via 192.168.120.3, 03:40:09
      192.168.100.0/32 is subnetted, 1 subnets
B        192.168.100.1 [200/0] via 192.168.120.3, 03:40:09
      192.168.200.0/32 is subnetted, 1 subnets
S        192.168.200.1 [1/0] via 10.22.80.2
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ip route' +++
show ip route
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area 
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override, p - overrides from PfR

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 3 subnets, 2 masks
C        10.1.2.0/24 is directly connected, Ethernet0/0
L        10.1.2.2/32 is directly connected, Ethernet0/0
O        10.1.3.0/24 [110/20] via 10.1.2.1, 03:41:08, Ethernet0/0
      192.168.120.0/32 is subnetted, 3 subnets
O        192.168.120.1 [110/11] via 10.1.2.1, 03:41:08, Ethernet0/0
C        192.168.120.2 is directly connected, Loopback0
O        192.168.120.3 [110/21] via 10.1.2.1, 03:41:08, Ethernet0/0
      192.168.146.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.146.0/24 is directly connected, Ethernet0/1.2
L        192.168.146.60/32 is directly connected, Ethernet0/1.2
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show ipv6 route' +++
show ipv6 route
MPLS-CORE-PE1#
Could not learn <class 'genie.libs.parser.iosxe.show_routing.ShowIpv6RouteDistributor'>
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Routing'                                                                                                            |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.iosxe.show_vrf.ShowVrfDetail'>                                                                                    |
|   cmd: <class 'genie.libs.parser.iosxe.show_routing.ShowIpRouteDistributor'>, arguments: {'vrf':'ford'}                                            |
|   cmd: <class 'genie.libs.parser.iosxe.show_routing.ShowIpRouteDistributor'>, arguments: {'vrf':''}                                                |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.iosxe.show_routing.ShowIpv6RouteDistributor'>, arguments: {'vrf':''}                                              |
|====================================================================================================================================================|
