+++ MPLS-CORE-PE1 with via 'cli': executing command 'show standby all' +++
show standby all
MPLS-CORE-PE1#
+++ MPLS-CORE-PE1 with via 'cli': executing command 'show standby delay' +++
show standby delay
Interface          Minimum Reload 
MPLS-CORE-PE1#
Could not learn <class 'genie.libs.parser.iosxe.show_standby.ShowStandbyDelay'>
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Hsrp'                                                                                                               |
+====================================================================================================================================================+
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.iosxe.show_standby.ShowStandbyAll'>                                                                               |
|   cmd: <class 'genie.libs.parser.iosxe.show_standby.ShowStandbyDelay'>                                                                             |
|====================================================================================================================================================|
